
FROM python:3
ADD instacart_spider.py /
COPY . .
COPY requirements.txt .
RUN pip3 install -r requirements.txt
CMD python3 ./instacart_spider.py