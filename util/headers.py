def get_headers(page,cookie):
    if page == 'storefront':
        return {
            'authority': 'www.instacart.com',
            'accept': 'application/json',
            'x-client-identifier': 'web',
            'x-csrf-token': 'FmC+e9zos1ysga5j5WHa465TdbgdT9L4phLw524FligdBl6Ve0vwGqBtCXmpHvRQMzfMuroFIGB6NvxF14cHbA==',
            'x-requested-with': 'XMLHttpRequest',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
            'content-type': 'application/json',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-mode': 'cors',
            'sec-fetch-dest': 'empty',
            'referer': 'https://www.instacart.com/store/wegmans/storefront',
            'accept-language': 'el,en-US;q=0.9,en;q=0.8',
            'cookie': cookie
            }