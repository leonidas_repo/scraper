import json

def get_departments(json_text):
    department_urls = {}
    for department in json.loads(json_text)['container']['modules']:
        try:
            department_name = department['data']['header']['label']
            department_url = department['async_data_path']
            department_urls[department_name] = department_url
        except TypeError as t:
            print("The following exception occured trying to decode json_text",t)
        except KeyError as k:
            print("Key",k,"not found in the dictionary")
    return department_urls

def get_products(json_text):
    block = []
    try:
        for item in json.loads(json_text)['module_data']['items']:
            block.append(item['name'])
    except TypeError as t:
        print("The following exception occured trying to decode json_text",t)
    except KeyError as k:
        print("Key",k,"not found in the dictionary")
    return block

def get_store_name(json_text):
    block = []
    try:
        return json.loads(json_text)['container']['title']
    except TypeError as t:
        print("The following exception occured trying to decode json_text",t)
    except KeyError as k:
        print("Title not found in the dictionary")