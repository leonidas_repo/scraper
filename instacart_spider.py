import requests
import util.headers
import util.data
import os
import json

cookie = ''

def get_session_cookie():
    try:
        global cookie
        cookie = prompt_user_for_cookie()
    except Exception as e:
        print(e)
        exit()
    return cookie

def prompt_user_for_cookie():
    cookie = input("Cookie from websocket failed. Please enter cookie from browser >> ")
    return cookie

def get_storefront():
    header = util.headers.get_headers('storefront',cookie)
    response = requests.get('https://www.instacart.com/v3/containers/wegmans/storefront', headers=header)
    if response.status_code != 200:
        print("storefront page failed.. Exiting")
        exit()
    return response.text

def get_block(department_name,department_url):
    header = util.headers.get_headers('storefront',cookie)
    response = requests.get(department_url, headers=header)
    if response.status_code != 200:
        print("storefront page failed for department",department_name)
    return response.text

def write_to_file(blocks):
    data_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),'data','data.json')
    with open(data_path, 'w', encoding='utf-8') as f:
        json.dump(blocks, f, ensure_ascii=False, indent=4)

def crawl():
    get_session_cookie()
    txt = get_storefront()
    departments = util.data.get_departments(txt)
    store_name = util.data.get_store_name(txt)
    blocks = {}
    for department_name,department_url in departments.items():
        department_txt = get_block(department_name,'https://www.instacart.com'+department_url)
        block_data = util.data.get_products(department_txt)
        blocks[department_name] = block_data
    blocks['store_name'] = store_name
    print(blocks)
    write_to_file(blocks)

crawl()