This is an instacart scrapper. It uses python 3 and requests to scrape content from the default store of choice
More precisely:

    - tries to login and interactively ascs for the login cookie if the attempts fails
    - Reads the store name and the departments from the first page
    - for every department scrapes the product block and return the product names
    - Additionally, saves the result as a json in the /data directory

Install the requirements `pip3 install -r requirements.txt` and run `python instacart_spider.py`
or use docker `docker build -t leonidas_instacart .`
and `docker run -ti leonidas_instacart` 